#!/usr/bin/python3

############### IMPORT LIBRARIES ##################################################################

import argparse
import atexit
import cmd
import json
import os
import readline
import shutil
import time
# import textual        # Coming soon! See https://pypi.org/project/textual-autocomplete/ for more
import warnings

from art import *
from datetime import datetime, timedelta
from enum import Enum
from os.path import basename, isdir, isfile
from pathlib import Path
from typing import Dict, List

############### CONSTANTS #########################################################################

class EventType(Enum):
    create_game               = "create_game"
    add_player                = "add_player"
    add_assassin              = "add_assassin"
    add_police                = "add_police"
    add_pseudonym             = "add_pseudonym"
    kill_player               = "kill_player"
    set_wanted                = "set_wanted"
    change_competency         = "change_competency"
    change_default_competency = "change_default_competency"
    begin_open_season         = "begin_open_season"
    revive_player             = "revive_player"

class ConfigOption(Enum):
    default_competency = "default_competency"
    game_name          = "game_name"

DEFAULT_SAVE_LOCATION = Path(__file__).parent.resolve().joinpath("save_data")
DEFAULT_SAVE = [
    {
        "date":       str(datetime.now()),
        "event_type": "create_game",
    }
]
DEFAULT_CONFIG = {
    "battle_royale":        False,
    "default_competency":   14,
    "game_name":            "year_term",
    "game_started":         False,
    "publish_incompetents": False,
    "publish_wanted":       True,
    "publish_stats":        False,
    "site_directory":       "./",
    "Wpedantic":            True,
}
LOCK_FILE = Path(__file__).parent.resolve().joinpath("assassins-autoumpire.lck")

############### LIBRARY CONFIG ####################################################################

parser = argparse.ArgumentParser(
    prog="AutoUmpire 2", description="CLI program for managing Assassins games"
)
parser.add_argument("-f", "--save-file", default=DEFAULT_SAVE_LOCATION)

readline.parse_and_bind("tab: complete")
readline.set_completer_delims("\t\r\n")

###################################################################################################


class Player:
    """
    The real identity of a person playing the game (as either a live player or police).
    """

    def __init__(self, id: int, name: str, email: str, college: str, address: str, notes: str):
        self.name = name
        self.email = email
        self.college = college
        self.address = address
        self.notes = notes

    def get_name(self):
        return self.name

    def __str__(self):
        return self.get_name()

    def __repr__(self):
        return str(self)


class Assassin:
    """
    An in-game object representing a player live-playing the game.
    """

    def __init__(self, player, initial_pseudonym: str):
        self.player = player
        self.pseudonyms = [initial_pseudonym]

        self.alive = True
        self.competency = { "deadline" : datetime.now() }
        self.wanted = {
            "wanted":                False,
            "reason":                "",
            "redemption_conditions": ""
        }

        self.conkers = 0

    def update_competency(self, deadline: datetime):
        self.competency_deadline = deadline

    def add_pseudonym(self, pseudonym: str):
        self.pseudonyms.append(pseudonym)

    def get_pseudonym(self, i: int):
        if i not in range(len(pseudonyms)):
            raise IndexError(
                f"Assassin {self.get_real_name()} does not have pseudonym [P{i}]"
            )

        return self.pseudonyms[i]

    def get_keyed_name(self):
        if self.alive:
            return f"{self.player.get_name()} (alive)"
        else:
            return f"{self.player.get_name()} (dead)"

    def get_real_name(self):
        return self.player.get_name()

    def set_wanted(self, wanted_status: bool, wanted_reason: str, redemption_conditions: str):
        """
        Sets a player's wanted status.
        """
        self.wanted = wanted_status

        if wanted_status:
            self.wanted = {
                "wanted": wanted_status,
                "reason": wanted_reason,
                "redemption_conditions": redemption_conditions
            }
        else:
            self.wanted = {
                "wanted": wanted_status,
                "reason": "",
                "redemption_conditions": ""
            }

    def __str__(self):
        return self.get_keyed_name()

    def __repr__(self):
        return str(self)

    @classmethod
    def from_dict(cls, player_dict: dict):
        return Player(
            player_dict["name"],
            player_dict["email"],
            player_dict["college"],
            player_dict["address"],
            player_dict["notes"]
        )

    def to_dict(self):
        player_dict = {
            "name":    self.name,
            "email":   self.email,
            "college": self.college,
            "address": self.address,
            "notes":   self.notes
        }

        return player_dict


class Police:
    """
    An in-game object representing a player playing as Police.
    """

    def __init__(self, initial_pseudonym: str):
        self.pseudonyms = []


class GameEvent:
    """
    An object representing an in-game event.
    """

    def __init__(self, date, headline: str, participants):
        self.date         = date
        self.headline     = headline
        self.participants = participants
        self.reports      = []

    def add_report(self, assassin: Union[Assassin, Police], report: str):
        report = Report(assassin, report)
        self.reports.append(report)

    @classmethod
    def from_dict(cls, event_dict: dict):
        raise Exception("Not implemented")

    def to_dict(self):
        event_dict = {
            "date":         self.date,
            "headline":     self.headline,
            "participants": [(a.get_keyed_name(), n) for (a, n) in self.participants]
        }

        return event_dict

    def get_headline(self):
        return self.headline

    def __str__(self):
        return f"Event ({self.get_headline()})"

    def __repr__(self):
        return str(self)


class GameReport:
    def __init__(self, assassin, report):
        self.assassin = assassin
        self.report   = report


class GameState:
    """
    An object representing a game's internal state. The state consists of players and their status, events, reports and kills.
    """

    def __init__(self):
        self.list_players      = {}
        self.list_live_players = {}
        self.list_dead_players = {}
        self.list_police       = {}

        self.list_events       = []

        atexit.register(self.save_events)

        # Config options
        self.config            = {}

    def get_player_by_name(self, real_name: str):
        """
        Returns the assassin or police corresponding to a player's real name.

        Note that the real name must end with one of (alive), (dead) or (police), as a player may have both an assassin and a police identity.

        Raises: IndexError
        """
        if real_name.endswith("(alive)"):
            real_name = real_name.removesuffix(" (alive)")
            return self.list_live_players[real_name]

        elif real_name.endswith("(alive)"):
            real_name = real_name.removesuffix(" (dead)")
            return self.list_dead_players[real_name]

        elif real_name.endswith("(police)"):
            real_name = real_name.removesuffix(" (police)")
            return self.list_police[real_name]

        else:
            raise AssertionError(
                "You must append '(alive)', '(dead)' or '(police)' to the player name to search for them"
            )

    def get_search_index(
        include_live_players: bool = True,
        include_dead_players: bool = True,
        include_police: bool = True,
    ):
        options = []

        if include_live_players:
            options += [f"{p.get_name()} (alive)" for p in self.get_live_players()]
        if include_dead_players:
            options += [f"{p.get_name()} (dead)" for p in self.get_dead_players()]
        if include_police:
            options += [f"{p.get_name()} (police)" for p in self.get_police()]

        return options

    def search_name(
        include_live_players: bool = True,
        include_dead_players: bool = True,
        include_police: bool = True,
    ) -> Player:
        """
        Provides a prompt to search for a player.
        """
        raise Exception("Not implemented")

        players = self.get_search_index(
            include_live_players, include_dead_players, include_police
        )

        def completer(text, state):
            options = [p for p in players if p.startswith(text)] + [None]
            return options[state]

        readline.set_completer(completer)

        print(
            "Please select a player (or type 'cancel' to cancel). Use tab to autocomplete."
        )
        cmd = input("> ")
        if cmd.lower() == "cancel":
            raise KeyboardInterrupt(
                "Operation cancelled by user"
            )  # TODO: Replace this with a custom 'CancelledException'
        return self.get_player_by_name(cmd)

    def create_event():
        raise Exception("Not implemented")

    def load_save_data(self, file: str = DEFAULT_SAVE_LOCATION):
        print("Loading game save...")
        try:
            if not isdir(file):
                file = file.parent.resolve()

            self.save_file = file.joinpath("assassins_save.jsonl")
            self.config_file = file.joinpath("config.json")

            self.events = self.load_events(self.save_file)
            self.configure_state()

            config_file_data = self.load_config(self.config_file)

            if self.config != config_file_data:
                self.add_event_edit_config(config_file_data)

            print("Done.")

        except FileNotFoundError:
            print("No game save found.\n")
            self.create_save_data(file)

    def create_save_data(self, file: str = None):
        print("Would you like to create a new Assassins game? [Y/n]")
        ans = input("> ").lower()
        if "n" in ans or not "y" in ans:
            exit()

        print(
            f"Please select a location for game data to be stored (leave blank for default of {DEFAULT_SAVE_LOCATION})."
        )
        ans = Path(input("> ") or DEFAULT_SAVE_LOCATION)

        ans.mkdir(exist_ok=True)

        self.save_file = ans.joinpath("assassins_save.jsonl")
        self.config_file = ans.joinpath("config.json")

        print(f"Writing save file to {self.save_file}...")
        self.events = DEFAULT_SAVE
        self.save_events()
        print("Done.")

        print(f"Writing config file to {self.config_file}...")
        with open(self.config_file, "w") as f:
            json.dump(DEFAULT_CONFIG, f)

        print("Done.")

        print(
            f"Game created successfully. Consider checking the file config at {self.config_file} before starting the game."
        )
        exit()

    def configure_state(self):
        for event in self.list_events:
            match event["event_type"]:
                case "create_game":
                    pass

                case "start_game":
                    self.config.get("game_started") = True

                case "add_player":
                    self.event_add_player()

                case "add_pseudonym":
                    self.event_add_pseudonym(event)

                case "add_kill":
                    raise NotImplementedError(f"Event type  {event['event_type']} not implemented")

                case "create_targeting_graph":
                    raise NotImplementedError(f"Event type  {event['event_type']} not implemented")

                case "send_emails":
                    raise NotImplementedError(f"Event type  {event['event_type']} not implemented")

                case "bookmark":
                    pass

                case "add_assassin":
                    self.event_add_assassin(event)

                case "edit_assassin":
                    raise NotImplementedError(f"Event type  {event['event_type']} not implemented")

                case "update_competency":
                    raise NotImplementedError(f"Event type  {event['event_type']} not implemented")

                case "edit_config":
                    self.event_edit_config(event)

                case "add_pseudonym":
                    self.event_add_pseudonym(event)

                case "add_event":
                    raise NotImplementedError(f"Event type  {event['event_type']} not implemented")

                case "edit_event":
                    raise NotImplementedError(f"Event type  {event['event_type']} not implemented")

                case "add_report":
                    raise NotImplementedError(f"Event type  {event['event_type']} not implemented")

                case "add_kill":
                    raise NotImplementedError(f"Event type  {event['event_type']} not implemented")

                case "new_week":
                    pass

                case "update_targeting_graph":
                    raise NotImplementedError(f"Event type  {event['event_type']} not implemented")

                case "publish_pages":
                    pass

                case "set_wanted":
                    self.event_set_wanted(event)

                case _:
                    raise ValueError(f"Event type  {event['event_type']} does not exist")

            self.validate()

    def add_event_edit_config(self, config: dict):
        self.log_event({"event_type": "edit_config", "date": str(datetime.now())} | self.config | config)

        self.config.update(config)
        self.save_config(config=config)

    def event_edit_config(self, event: dict):
        updated_config_options = {k:v for k,v in event if k != "event_type" and k != "date"}
        self.config.update(updated_config_options)

    def event_add_assassin(self, event: dict):
        name = event["name"]
        pseudonym = event["pseudonym"]
        police = event["police"]

        try:
            player = self.list_players_by_name[name]

            if police:
                assassin = Police(player, pseudonym)
                raise Exception("Not implemented")

            else:
                assassin = Assassin(player, pseudonym)
                self.list_live_players[assassin.get_keyed_name] = assassin

        except KeyError:
            raise KeyError(f"Player {player} does not exist")

    def event_add_player(self, event: dict):
        player = Player.from_dict(event)
        self.list_players[player.get_name()] = player

    def event_add_pseudonym(self, event: dict):
        assassin = get_player_by_name(event.get("player"))
        pseudonym = event.get("pseudonym")

        assassin.add_pseudonym(pseudonym)

    def event_set_wanted(self, event: dict):
        assassin = get_player_by_name(event.get("player"))

        assassin.set_wanted(event.get("wanted"), event, event["reason"], event["redemption_conditions"])

    def event_update_competency(self, event: dict):
        assassin = self.get_player_by_name(event.get("player"))
        assassin.update_competency(event.get("deadline", datetime.now() + timedelta(days = self.config.get("default_competency"))))

    def save_config(self, config_file: str = None, config: dict = None) -> dict:
        if config_file is None:
            config_file = self.config_file

        if config is None:
            config = self.config

        with open(config_file, "w") as f:
            json.dump(config, f, indent = 4)

        self.create_backup(config_file=self.config_file)

    def load_config(self, config_file: str = None) -> dict:
        if config_file is None:
            config_file = self.config_file

        with open(config_file, "r") as f:
            config = json.load(f)

        return config

    def save_events(self, save_file: str = None, events: List[dict] = None):
        if save_file is None:
            save_file = self.save_file

        if events is None:
            events = self.events

        with open(self.save_file, "w") as f:
            json.dump(self.events, f, indent = 4)

        self.create_backup(save_file=self.save_file)

    def load_events(self, save_file: str = None) -> List[dict]:
        if save_file is None:
            save_file = self.save_file

        with open(self.save_file, "r") as f:
            events = json.load(f)

        return events

    def create_backup(self, save_file: str = None, config_file: str = None):
        """
        Backs up the assassins_save.jsonl and config.json files.
        """

        if save_file is None:
            save_file = self.save_file
        if config_file is None:
            config_file = self.config_file


        backup_dir = save_file.parent.resolve().joinpath("backups")
        backup_dir.mkdir(exist_ok=True)

        time = datetime.now()

        print("Hello")
        print(backup_dir.joinpath(f"save_{time}.jsonl.bak"))

        shutil.copyfile(save_file, backup_dir.joinpath(f"save_{time}.jsonl.bak"))
        shutil.copyfile(config_file, backup_dir.joinpath(f"config_{time}.json.bak"))

    def add_player(self, name: str, email: str, college: str, address: str, notes: str, date: datetime = None):
        if datetime is None:
            datetime = datetime.now()

        data = {"event_type": "add_player", "real_name": name, "email": email, "college": college, "address": address, "notes": notes, "date": datetime}
        self.log_event(data)
        self.configure_state()

    def log_event(self, data):
        self.events.append(data)
        self.save_events()

    def validate(self):
        """
        Validates the game state and raises a ValueError if the game state is incorrect.
        """
        raise Exception("Not implemented")


def select_command():
    commands = {
        "1": add_player,
        "Add Player": add_player,
        "Add player": add_player,
        "add player": add_player,
        "2": None,
        "Add Assassin or Police": None,
        "3": None,
        "Edit Assassin or Police": None,
        "4": None,
        "Add event": None,
        "5": None,
        "Edit event": None,
        "6": None,
        "Add report": None,
        "7": None,
        "Add pseudonym": None,
        "8": None,
        "Back up the game state": None,
        "9": None,
        "Edit the config options": None,
        "0": exit,
        "Exit": exit,
        "exit": exit,
    }

    print(
        "Please type a command. Use tab to autocomplete.\n\n"
        "  1) Add Player\n"
        "  2) Add Assassin or Police\n"
        "  3) Edit Assassin or Police\n"
        "  4) Add event\n"
        "  5) Edit event\n"
        "  6) Add report\n"
        "  7) Add pseudonym\n"
        "  8) Back up the game state\n"
        "  9) Edit the config options\n"
        "  0) Exit\n"
    )

    def completer(text, state):
        options = list(commands.keys()) + [None]
        return options[state]

    readline.set_completer(completer)

    cmd = input("> ")

    commands[cmd]()


def print_title():
    tprint("AutoUmpire   V2")


def get_lock():
    if isfile(LOCK_FILE):
        print(f"{LOCK_FILE} is currently being held by another process. This is probably because somebody else is running another copy of this program.")
        print(f"Running multiple copies of this program simultaneously can cause concurrency issues. If you are sure that nobody else is using the program, you may delete {LOCK_FILE.name}.")
        print(f"If you want to come back to this program later, you can press Ctrl-C to exit.")
        while isfile(LOCK_FILE):
            time.sleep(1)

    with open(LOCK_FILE, "w") as f:
        f.write("")

    atexit.register(release_lock)


def release_lock():
    try:
        os.remove(LOCK_FILE)

    except FileNotFoundError:
        raise RuntimeWarning(f"{LOCK_FILE.name} was removed during the program's execution. While this does not present a problem by itself, other users may have been using the program whilst you were running it. To prevent errors, it is recommended that you check the contents of {args.save_file} for any unintended side effects.")


if __name__ == "__main__":
    print_title()

    args = parser.parse_args()

    get_lock()

    game_state = GameState()
    game_state.load_save_data(args.save_file)

    game_state.create_backup()

    while True:
        select_command()
