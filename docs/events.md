An event represents any notable occurrence, incident or battle which occurs during the game. Events may be state-changing (such as when a player kills another player or goes wanted), or they may not be (such as when two Assassins encounter and fail to kill each other). Events are published on the game website for all players to see, and players may write reports for them.

# Structure

An event is logged in `assassins_save.jsonl` in the form:

```
{
    "event_type": "add_event",
    "date":       "2024-03-15 13:41:34.108370",
    "players":    [ ("Alice Smith (alive)", 0) ],
    "headline":   "[P0] forgot to enter a headline!"
}
```

Each player in the `players` entry takes the form `(player_name, n)`, where `player_name` is the Asasssin's real name (suffixed with either `"(alive)"`, `"(dead)"` or `"(police)"`) and `n` is the index of the pseudonym they've used for the event (indexed from `0`).

# Headlines

Each event has an associated headline summarising the event. Headlines are featured on the game website, so they should contain sufficient information for readers to understand what occurred. At a minimum, they should include the pseudonyms of all players involved, as well as which players were killed.

## Pseudonym templates

While pseudonyms can be entered verbatim, AutoUmpire also provides templates, which are automatically expanded into player names and pseudonyms when the game pages are generated. These templates offer the advantage of colouring players' pseudonyms when generating the game pages, and TODO

AutoUmpire supports the following macros for headlines:

* `[P0]`, `[P1]`, etc. to refer to an Assassin's pseudonym. For example, `[P0]` might expand to `"My Awesome Pseudonym"`.
* `[L0]`, `[L1]`, etc. to refer to an Assassin's list of pseudonyms along with their name. For example, `[L0]` might expand to `"My Third Pseudonym AKA My Second Pseudonym AKA My First Pseudonym (Alice Smith)"`. This is useful for revealing a player's identity after they are killed.
* `[N0]`, `[N1]`, etc. to refer to a player's real name. For example, `[N0]` might expand to `"Alice Smith"`. `[L0]` should be preferred over this wherever possible.

AutoUmpire will show a warning if `[Pn]` is used for a dead player, or if `[Ln]` is used for a live player or in player reports.

# Reports

Each player may submit a report for an event, which provides their perspective of the event. A player's report will be written in the same colour as their pseudonym, with the exception of other players' pseudonyms, which are written in their respective colours.

Reports take the form:

```
Avatar of Wrath reports:

    Something is wrong. America!
```

Care should be taken by the Umpires to ensure that sensitive information (such as names and locations) is redacted from reports. Normally, this is done by replacing the sensitive text with `"[REDACTED]"`, or by replacing names with pseudonyms. In particular, reports should favour gender-ambiguous pronouns to avoid leaking players' genders.

The game website does provide support for special features (such as images, custom fonts, and custom colours), but these need to be handled manually as AutoUmpire does not provide support for these at present.

## Pseudonym templates

As with headlines, reports support pseudonym templates, which are automatically expanded into player names and pseudonyms.

AutoUmpire supports the following macros for reports:

* `[P0]`, `[P1]`, etc. to refer to an Assassin's pseudonym. For example, `[P0]` might expand to `"My Awesome Pseudonym"`.
* `[N0]`, `[N1]`, etc. to refer to a player's real name. For example, `[N0]` might expand to `"Alice Smith"`. `[L0]` should be preferred over this wherever possible.

AutoUmpire will show a warning if `[Nn]` is used in reports. A `ValueError` will be raised if `[Ln]` is used.
