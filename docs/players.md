Players can participate in the game as either live Assassins or Police. Each has different

# Players

A player represents a (real) person who's playing the game. All Assassins and Police are attached to a Player, so a player must be created before either of these can be created.

## Properties

A player has the following properties:

* `name`: The player's real name.
* `email`: The player's email address (preferably a university address).
* `college`: The player's college. This is only relevant for collegiate universities; it can be set to any placeholder value for other settings.
* `address`: The player's address. This address will be given to other players, so this should normally be a university address.
* `water_status`: Whether water weapons can be used in their room. Can be either `"No water"`, `"Water with care"` or `"Full water"`.
* `notes`: Any notes provided by the player. This may include any conditions under which the player is out of bounds, nicknames and preferred pronouns, or any other information which other players should be aware of.

## Creating a player

A player can be created using the following event:

```
{
    "event_type":   "add_player",
    "date":         "...",
    "name":         "...",
    "email":        "...",
    "college":      "...",
    "address":      "...",
    "water_status": "No water"
    "notes":        "Out of bounds between midnight and 7am."
}
```

## Editing a player

A player can be edited by editing their corresponding `add_player` event in `assassins_save.jsonl`.

## Deleting a player

A player can be deleted by removing their corresponding `add_player` event from `assassins_save.jsonl`. This should only be done if the player has no corresponding Assassin or Police, or their corresponding Assassin or Police hasn't been involved in any events. If the `Wpedantic` option is set in `config.json`, the player's corresponding Assassin or Police must also be removed from `assassins_save.jsonl`.

# Assassins

## Creating an Assassin

An Assassin can be created using the following event:

```
{
    "event_type": "add_assassin",
    "date":       "...",
    "name":       "...",
    "pseudonym":  "...",
    "police":     "false"
}
```

## Editing an Assassin

An Assassin can be edited by using the `edit_assassin` event.

```
{
    "event_type":            "edit_assassin",
    "date":                  "...",
    "name":                  "... (alive/dead)",
    "police":                "...",
    "competency":            "...",
    "wanted":                "...",
    "redemption_conditions": "...",
    "score":                 ...
}
```

Each of the `competency`, `wanted`, `redemption_conditions` and `score` keys is optional, and any values not specified will be left unchanged. An Assassin's initial pseudonym must be changed by editing the corresponding `add_assassin` event.

## Deleting an Assassin

The only way of deleting an Assassin is by removing the corresponding `add_assassin` event from `assassins_save.jsonl`. This should only be done if the Assassin hasn't been involved nin any events.

## Pseudonyms

### Adding a pseudonym

### Editing a pseudonym

### Removing a pseudonym

## Competency

All live Assassins are given a competency deadline, and are required participate in the game (normally by making 1 licit kill or 2 attempts) to remain competent. Assassins who don't do anything are placed on the Incompetents list, and become valid targets for all live Assassins and Police. Furthermore, towards the end of the game, many Umpires choose to thunderbolt all players who haven't done anything in the entire game. The idea of competency is to give Assassins an incentive to participate actively throughout the game.

Players who make 1 licit kill or 2 attempts have their competency deadline set to the current time plus the `default_competency` value specified in `config.json`. This is typically set to `7` at the start of the game and decreases periodically as the game progresses. Players will periodically receive an email reminding them of their competency deadline.

### Editing competency deadline

A player's competency deadline can be changed by using the `update_competency` event.

```
{
    "event_type": "update_competency",
    "date":       "...",
    "name":       "... (alive)"
    "deadline":   "...",
}
```

The `deadline` key can be omitted, in which case the player's new deadline will be set to the default value for the provided event date.

```
{
    "event_type": "update_competency",
    "date":       "...",
    "name":       "..."
}
```

### Setting competency status

AutoUmpire will automatically send an Assassin Incompetent (and publish them on the Incompetents list if `publish_incompetents` is set in `config.json`) if their competency deadline is in the past. Additionally, an Assassin can be sent Incompetent immediately by using the `update_competency` event to set their competency deadline to a date in the past.

Similarly, an Incompetent Assassin can be removed from the Incompetents list by using the `update_competency` flag to set their competency deadline to a future date.

# Police

Each player in the `players` entry takes the form `(player_name, n)`, where `player_name` is the Asasssin's real name (suffixed with either `"(alive)"`, `"(dead)"` or `"(police)"`) and `n` is the index of the pseudonym they've used for the event (indexed from `0`).

# Live assassins

