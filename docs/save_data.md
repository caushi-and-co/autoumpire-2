# Introduction

AutoUmpire 2 stores data in two files: `config.json` and `assassins_save.jsonl`. By default, these are created in the `save_data` directory, but this can be changed using the `--save-file` flag. Note that `--safe-file` must point to a directory containing these two files: if either file is selected, AutoUmpire will attempt to use the parent directory as the save location.

## Creating a new save game

When AutoUmpire is first launched, it checks that these two files exist. If not, it will attempt to create a directory for you. If AU2 wants to create a new game, ensure that the folder doesn't contain either `config.json` or `assassins_save.jsonl`, as these will be overwritten by AutoUmpire.

```
$ ./assassins-autoumpire.py
    _            _           _   _                    _                __     __ ____
   / \    _   _ | |_   ___  | | | | _ __ ___   _ __  (_) _ __   ___    \ \   / /|___ \
  / _ \  | | | || __| / _ \ | | | || '_ ` _ \ | '_ \ | || '__| / _ \    \ \ / /   __) |
 / ___ \ | |_| || |_ | (_) || |_| || | | | | || |_) || || |   |  __/     \ V /   / __/
/_/   \_\ \__,_| \__| \___/  \___/ |_| |_| |_|| .__/ |_||_|    \___|      \_/   |_____|
                                              |_|

Loading game save...
No game save found.

Would you like to create a new Assassins game? [Y/n]
> Y
Please select a location for game data to be stored (leave blank for default of <...>/save_data).
>
Writing save file to <...>/save_data/assassins_save.jsonl...
Done.
Writing config file to <...>/save_data/config.json...
Done.
Game created successfully. Consider checking the file config at <...>/save_data/config.json before starting the game.
```

The directory will contain sample `config.json` and `assassins_save.jsonl` files containing the base information needed to start the game.

## Game configuration

### `battle_royale`

Determines whether the targeting graph should used. If set to `true`, players will not be sent emails listing their targets, and the list of players will instead be published on the game website. Defaults to `false`.

Set this to `true` for open season and May Week games, and to `false` otherwise. While it is possible to change this value from `false` to `true` mid-game (eg. to begin open season), be aware that changing it from `true` to `false` can unexpectedly send players wanted.

### `default_competency`

The default competency extension in days that will be granted to players. Defaults to `14`.

Normally, you should only use the default value while setting up the game (to ensure that players start with an increased competency period at the start of the game). After adding all players at the start of the game, you should change this to `7`. As the game progresses, you might consider reducing this value further to increase the pace of the game.

Note that the default competency extension can always be modified in `assassins_save.jsonl`.

### `game_name`

The game name for indexing and archival purposes. The recommended format is `year_term`. Defaults to `"year_term"` (which intentionally raises a `ValueError`).

### `game_started`

Whether the game is live and should be published. Defaults to `false`.

### `publish_incompetents`

Determines whether AutoUmpire should publish the Incompetents list. Defaults to `false`.

Normally, you should leave this as `false` until the Incobash, then switch it to `true` at the start of the Incobash. The intention of this is so that the Incompetents list first goes live in time for the Incobash.

### `publish_wanted`

Determines whether AutoUmpire should publish the Wanted list. Defaults to `true`.

### `publish_stats`

TODO

### `site_directory`

The base directory of the game website. For most webservers, this should be either a `public_html` or `www` directory.

Must be a fully qualified directory (ie. starting with a `/`). Relative directories (those starting `./`) will raise a `ValueError`.

### `Wpedantic`

Whether warnings should instead raise Exceptions. This is useful for preventing unexpected side-effects.

## Game data

Takes the form:

```
[
    { "event_type": ..., "date": ..., ... },
    { "event_type": ..., "date": ..., ... },
    ...
]
```


### `create_game`

Creates the game. This must always be the first event.

Arguments:

* `date`: The event date.

Example:

```
{
    "event_type": "create_game",
    "date":       "2024-03-15 13:41:34.108370"
}
```

### `start_game`

Starts the game, sets the `game_started` value in `config.json`, publishes the initial game pages, and sends out the initial batch of targeting emails to all players. All live Assassins must be added and `create_targeting_graph` must be called before this event.

Arguments:

* `date`: The event date.

Example:

```
{
    "event_type": "start_game",
    "date":       "2024-03-15 13:41:34.108370"
}
```

### `create_targeting_graph`

Builds the initial targeting graph, setting each live Assassin's targets. Police players are unaffected by this event. All live Assassins must be added before this event, as Assassins added afterwards will not have targets assigned.

Arguments:

* `date`: The event date.
* (Optional) `force_targets`: A list of pairs of player names (suffixed with `"alive"`), of the form `("targeter (alive)", "target (alive)")`. Used to control the targets that one or more players get. Defaults to `[]`.

Example:

```
{
    "event_type": "create_targeting_graph",
    "date":       "2024-03-15 13:41:34.108370"
}
```

### `send_emails`

Sends out emails to players listing their targets and competency status.

Arguments:

* `date`: The event date.
* (Optional) `recipients`: Who to send the email to. Can be either `"updated_players_only"` for players whose targets and/or competency deadlines have changed, `"live_assassins_only"` for all live Assassins, `"police_only"` for all Police players, or `"all"` for all live and Police players. Defaults to `"updated_players_only"`.

```
{
    "event_type": "send_mails",
    "date":       "2024-03-15 13:41:34.108370"
    "recipients": "updated_players_only"
}
```

### `bookmark`

The `bookmark` event does not do anything by itself, but is used to keep track of which side-effecting events have been executed by AutoUmpire. Namely, each time AutoUmpire is started, it sends out all emails since the last `bookmark` event, and a new `bookmark` event is logged.

Arguments:

* `date`: The event date.

Example:

```
{
    "event_type": "bookmark",
    "date":       "2024-03-15 13:41:34.108370"
}
```

### `add_player`

Adds a player to the game. Note that this does not register them as either a live Assassin or as Police: this must be done separately using `add_assassin`.

Arguments:

* `date`: The event date.
* `name`: The player's name.
* `email`: The player's email address (preferably a university email address).
* `college`: The player's college.
* `address`: The player's address.
* `water_status`: The player's address' water weapon status. Can be either `"No water"`, `"Water with care"` or `"Full water"`.
* `notes`: The player's notes.

Example:

```
{
    "event_type":   "add_player",
    "date":         "2024-03-15 13:41:34.108370",
    "name":         "Alice Smith",
    "email":        "a.smith@cam.ac.uk",
    "college":      "Christ's College",
    "address":      "Old Court A1",
    "water_status": "No water"
    "notes":        "Out of bounds between midnight and 7am."
}
```

### `add_assassin`

This event registers a player as either a live Assassin or a Police player. The player must be added beforehand using `add_player`.

If the player provided doesn't exist, no Assassin or Police will be created, and a warning will be shown.

Arguments:

* `date`: The event date.
* `name`: The player's name.
* `pseudonym`: The player's initial pseudonym.
* `police`: Whether to register the player as Police. If set to `false`, the player will be registered as a live Assassin.
* (Optional) `alive`: Whether to register the player as alive. Defaults to `true`
* (Optional) `track_stats`: Whether AutoUmpire should track the player's stats. If set to `true`, the player's score increases as they score kills, and their score will be listed on the leaderboard at the end of the game. Defaults to `true`.
* (Optional) `starting_score`: The player's starting score. Defaults to `0`.

Example:

```
{
    "event_type": "add_assassin",
    "date":       "2024-03-15 13:41:34.108370",
    "name":       "Alice Smith",
    "pseudonym":  "Avatar of Wrath",
    "police":     "false"
}
```

### `edit_assassin`

Edits an Assassin's properties. Any properties specified by this event type will be changed to the values given, while all other values will be left unchanged.

Arguments:

* `date`: The event date.
* `name`: The player's name, suffixed with `"(alive)"`, `"(dead)"` or `"(police)"`.
* (Optional) `competency`: The player's new competency extension.
* (Optional) `wanted`: Whether the player is Wanted.
* (Optional) `redemption_conditions`: The player's redemption conditions.
* (Optional) `score`: The player's new score.

Example:

```
{
    "event_type":   "edit_assassin",
    "date":         "2024-03-15 13:41:34.108370",
    "name":         "Alice Smith (police)",
    "score":        30.7
}
```

### `update_competency`

Updates a player's competency deadline. If the player is Incompetent, additionally sets their competency status.

Arguments:

* `date`: The event date. To avoid ambiguity, the 'event' here refers to a log event, and not to an in-game event.
* `name`: The name of the player, suffixed with `"(alive)"` or `"(police)"`.
* (Optional) `deadline`: The new competency deadline. Defaults to `date` plus the `default_competency` value specified in `config.json`.

Example:

```
{
    "event_type": "update_competency",
    "date":       "2024-03-15 13:41:34.108370",
    "name":       "Alice Smith (alive)"
}
```

### `edit_config`

Updates `config.json`. Any properties specified by this event type will be changed to the values given, while all other values will be left unchanged.

Arguments:

* `date`: The event date.
* (Optional) `battle_royale`: Whether the targeting graph should be used. If set to `true`, the targeting graph will be ignored for the rest of the game, making it useful for starting open season.
* (Optional) `default_competency`: The default competency extension for players.
* (Optional) `publish_incompetents`: Whether to publish the Incompetents list.
* (Optional) `publish_wanted`: Whether to publish the wanted list.
* (Optional) `publish_stats`: Whether to publish stats.

Example:

```
{
    "event_type":    "edit_config",
    "date":          "2024-03-15 13:41:34.108370",
    "battle_royale": true
}
```

### `add_pseudonym`

Awards a pseudonym to a player. Traditionally, pseudonyms are given to players after every second kill, with no limit to the number of pseudonyms that can be acquired. The pseudonym is added to the end of the player's list of pseudonyms, so that players' pseudonyms are listed in order of acquisition.

Arguments:

* `date`: The event date. To avoid ambiguity, this refers to the date of the log event rather than any in-game event.
* `name`: The name of the player who made the kill, suffixed with either `"(alive)"` or `"(police)"`.
* `pseudonym`: The pseudonym to award the player. Must be unique (either among live/dead Assassins or among Police, but the same player can have the same pseudonym as both a live Assassin and a Police).

Example:

```
{
    "event_type": "add_pseudonym",
    "date":       "2024-03-15 13:41:34.108370",
    "name":       "Alice Smith (alive)",
    "peudonym":   "Avatar of Wrath"
}
```

### `add_event`

Adds an event and an associated headline. This event will be published onto the game website.

Note that events should be added _after_ any kills are registered so that the headline displays pseudonyms in the correct colours.

Arguments:

* `date`: The event date.
* `players`: A list of the players involved. Each player should be provided in the form `(player_name, n)`, where `player_name` is suffixed with either `"(alive)"`, `"(dead)"` or `"(police)"`, and `n` is the index of the pseudonym (starting from `0`).
* `headline`: The event headline.

Example:

```
{
    "event_type": "add_event",
    "date":       "2024-03-15 13:41:34.108370",
    "players":    [ ("Alice Smith (alive)", 0) ],
    "headline":   "[P0] forgot to enter a headline!"
}
```

### `edit_event`

Updates an event's properties. Normally, events should be edited by editing `assassins_save.jsonl` directly, but this event type is useful if the Umpires need to rule that a player should have died long after the event has taken place, so that AutoUmpire can kill the player without needing to recalculate the entire targeting graph.

Arguments:

* `date`: The event date.
* `event`: The 0-indexed index of the event to be modified.
* (Optional) `"players"`: The new list of players involved. This must include all players added in the previous instance of the event. Note that, if a player previously suffixed with `"(alive)"` is now dead, they should now be suffixed with `"(dead)"`.
* (Optional) `"headline"`: The new headline.

```
{
    "event_type": "edit_event",
    "date":       "2024-03-15 13:41:34.108370",
    "players":    [ ("Alice Smith (dead)", 0), ("Bob Jones (police)", 1) ]
}
```

### `add_report`

Adds a report to an event. This report will be published under the player's pseudonym onto the game website.

Arguments:

* `date`: The event date.
* `name`: The name of the player making the report, suffixed with `"(alive)"`, `"(dead)"` or `"(police)"`.
* `report`: The textual report being made.

Example:

```
{
    "event_type": "add_report",
    "date":       "2024-03-15 13:41:34.108370",
    "name":       "Alice Smith (police)",
    "report":     "Press START to continue."
}
```

### `add_kill`

This event registers a kill made by a player. When registered, the player is marked as dead and removed from the game, and their score is transferred to the player who made the kill if possible. This event should be used _before_ the corresponding event, so that the headline displays pseudonyms in the correct colours.

Arguments:

* `date`: The event date.
* `killer`: The name of the player who made the kill, suffixed with `"(alive)"` or `"(police)"`. The name given can also be `""`, in which case the killed player is considered to have been thunderbolted.
* `victim`: The name of the killed player, suffixed with `"(alive)"` or `"(police)"`. The name given can also be `""`, in which case nobody is considered to have died (which can be useful when awarding bonus points to a player).
* (Optional) `transfer_points`: Whether to award the victim's points to the player who made the kill. Defaults to `true`. Consider setting this to `false` for illicit kills. Has no effect when either player is Police.
* (Optional) `award_points`: The number of additional points to award to the player who made the kill. Stacks additively with `transfer_points`. Defaults to `0`.

Example:

```
{
    "event_type": "add_kill",
    "date":       "2024-03-15 13:41:34.108370",
    "killer":     "Alice Smith (police)",
    "victim":     "Bob Jones (alive)"
}
```

### `new_week`

This event marks the start of a new week. It does not update the game state _per se_, but it is used to separate each week's events when publishing game events.

Arguments:

* `date`: The event date.
* `week_number`: The new week number.

Example:

```
{
    "event_type":  "new_week",
    "date":        "2024-03-15 13:41:34.108370",
    "week_number": 4
}
```

### `update_targeting_graph`

Updates the targeting graph by welding edges together so that all players have 3 targets. It is recommended to use this after all kills for a given day are processed to maximise the amount of slack that AutoUmpire has to keep the targeting graph connected.

This event type does not need to be called after open season has started or in May Week games.

Arguments:

* `date`: The event date.
* (Optional) `force_targets`: A list of pairs of player names (suffixed with `"alive"`), of the form `("targeter (alive)", "target (alive)")`. Used to control the targets that one or more players get. Defaults to `[]`.

Example:

```
{
    "event_type": "update_targeting_graph",
    "date":       "2024-03-15 13:41:34.108370"
}
```

### `publish_pages`

Publishes all pages along with the Wanted and Incompetents lists (if configured).

* `date`:                            The event date.
* (Optional) `publish_wanted`:       Whether to publish the Wanted list. Defaults to the value specified in `config.json`.
* (Optional) `publish_incompetents`: Whether to publish the Incompetents list. Defaults to the value specified in `config.json`.

Example:

```
{
    "event_type": "publish_pages",
    "date":       "2024-03-15 13:41:34.108370"
}
```

### `set_wanted`

Sets a player's wanted status along with a reason and their redemption conditions. Aliases the `edit_assassin` event.

* `date`: The event date.
* `player`: The player's name, suffixed with `"(alive)"`, `"(dead)"` or `"(police)"`.
* `wanted`: The player's new wanted status. Setting this to `false` will override the `reason` and `redemption_conditions` arguments.
* `reason`: The reason the player was sent Wanted. Leave empty if `wanted` is `false`. Will be published on the game website.
* `redemption_conditions`: How a player can remove themselves from the Wanted list. Leave empty if `wanted` is `false`. Will be published on the game website.

Example:

```
{
    "event_type":            "set_wanted",
    "date":                  "2024-03-15 13:41:34.108370",
    "player":                "Alice Smith (alive)",
    "wanted":                true,
    "reason":                "Making an illicit kill",
    "redemption_conditions": "1 kill or 2 days survived"
}

{
    "event_type":            "set_wanted",
    "date":                  "2024-03-15 13:41:34.108370",
    "player":                "Alice Smith (alive)",
    "wanted":                false
}
```
